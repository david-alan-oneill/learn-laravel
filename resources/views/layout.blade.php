<!DOCTYPE html>
<html>
<head>
	<title>Layout page</title>
    <link href="/css/all.css" rel="stylesheet">
</head>
<body>
@include('partials.nav')

  <div class="container">

      @include('flash::message')

	@yield('content')
  </div>

  <script>
     // $('div.alert').not('.alert-important').delay(3000).slideUp(300);
      $('#flash-overlay-modal').modal();
  </script>

  <script src="/js/all.js"></script>
  @yield('footer')

</body>
</html>