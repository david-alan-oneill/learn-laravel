<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'body', 'published_at'];

    protected $dates = ['published_at'];

    public function scopePublished($query){
        $query->where('published_at','<=',Carbon::now());
    }

    public function scopeUnpublished($query){
        $query->where('published_at','>',Carbon::now());
    }

    public function setPublishedAtAttribute($date){

        $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function getPublishedAtAttribute($date){

        return Carbon::parse($date)->format('Y-m-d');
    }


    //An Article is owned by a user
    public function user(){
        return $this->belongsTo('App\User');
    }

    //get the tags associated with the given article
    public function tags(){
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    public function getTagListAttribute(){

        return $this->tags->lists('id');
    }

}
