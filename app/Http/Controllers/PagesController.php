<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	public function about(){
		//$name = 'David';

		$people = [
		'Bob','Jim','Jam'
		];

		return view('pages.about',compact('people'));
		// return view('pages.about')->with([
		// 	'first' => 'David',
		// 	'last' => 'O Neill'
		// 	]);
	}

	public function contact(){
		return view('pages.contact');
	}

}
