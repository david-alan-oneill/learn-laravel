<?php

use Illuminate\Database\Seeder;

class TriggerTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('trigger_table')->delete();

		DB::table('trigger_table')->insert(array(
			'id' => 1,
		 	'a' => 5.1,
		 	'b' => 6.5,
		 	'c' => 0,
		 	));
		DB::table('trigger_table')->insert(array(
			'id' => 2,
		 	'a' => 10,
		 	'b' => 8,
		 	'c' => 0,
		 	));
		// $this->call('UserTableSeeder');
	}

}
