<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerTablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trigger_table', function(Blueprint $table)
		{
			$table->increments('id');
			$table->double('a');
			$table->double('b');
			$table->double('c');
		});

		DB::unprepared('CREATE TRIGGER trigger2 BEFORE INSERT ON trigger_table FOR EACH ROW SET NEW.c = NEW.a * NEW.b');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
//		Schema::drop('trigger_table');
	}

}
